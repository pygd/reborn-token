# REBORN Token

리본 플랫폼 토큰

## 사양

- 토큰명: 리본(REBORN)
- 토큰 기호: REB
- 토큰 타입: ERC20
- 총 발행량: 1,000,000,000 REB
- 총 판매량: 300,000,000 REB
- 교환 가능 화폐: ETH

## 상세 사양

- REB 토큰은 자금 조정(minting/burning) 후 최종 동결 될 수 있습니다.
- 불순한 의도를 가진 사용 목적을 방지하기 위하여 각 계정은 격리 처분 될 수 있습니다.

## 개발 방향

안전한 ERC20 토큰 개발을 위해 [truffle](truffleframework.com) 프레임워크와 검증된 [open-zeppelin](openzeppelin.org) 라이브러리를 기반으로 필요한 기능을 추가 구현 합니다.
