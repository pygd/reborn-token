pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

contract QuarantinableToken is StandardToken, Ownable {
    struct Quarantine {
        bool quarantined;
        string reason;
    }

    mapping (address => Quarantine) quarantiners;
    event AccountQuarantined(address indexed operator, string reason);
    event AccountUnquarantined(address indexed operator, string reason);

    modifier whenNotQuarantined() {
        require(!quarantiners[msg.sender].quarantined, "The address is blocked");
        _;
    }

    function quarantined(address _account) public view returns (bool, string) {
        return (quarantiners[_account].quarantined, quarantiners[_account].reason);
    }

    function quarantine(address _account, string _reason) public onlyOwner {
        quarantiners[_account].quarantined = true;
        quarantiners[_account].reason = _reason;
        emit AccountQuarantined(_account, _reason);
    }

    function unquarantine(address _account, string _reason) public onlyOwner {
        quarantiners[_account].quarantined = false;
        quarantiners[_account].reason = _reason;
        emit AccountUnquarantined(_account, _reason);
    }

    function transfer(
        address _to,
        uint256 _value
    )
        public
        whenNotQuarantined
        returns (bool)
    {
        return super.transfer(_to, _value);
    }

    function transferFrom(
        address _from,
        address _to,
        uint256 _value
    )
        public
        whenNotQuarantined
        returns (bool)
    {
        return super.transferFrom(_from, _to, _value);
    }

    function approve(
        address _spender,
        uint256 _value
    )
        public
        whenNotQuarantined
        returns (bool)
    {
        return super.approve(_spender, _value);
    }

    function increaseApproval(
        address _spender,
        uint _addedValue
    )
        public
        whenNotQuarantined
        returns (bool success)
    {
        return super.increaseApproval(_spender, _addedValue);
    }

    function decreaseApproval(
        address _spender,
        uint _subtractedValue
    )
        public
        whenNotQuarantined
        returns (bool success)
    {
        return super.decreaseApproval(_spender, _subtractedValue);
    }
}